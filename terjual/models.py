from django.db import models
from inven.models import Makanan

# Create your models here.
class AddMakanan(models.Model):
    addmakanan = models.CharField(max_length=100)
    class Meta:
        ordering = ('addmakanan',)

    def __str__(self):
        return self.addmakanan


