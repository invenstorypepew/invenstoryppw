from django.contrib import admin
from django.urls import path
from . import views

app_name = 'terjual'

urlpatterns = [
    path('', views.sold, name = 'sold'),
]