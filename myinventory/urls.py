from django.urls import path
from . import views

app_name = 'myinventory'

urlpatterns = [
    path('', views.inventory, name='inventory'),
]