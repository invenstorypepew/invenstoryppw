[![pipeline status](https://gitlab.com/invenstorypepew/invenstoryppw/badges/master/pipeline.svg)](https://gitlab.com/invenstorypepew/invenstoryppw/commits/master) [![coverage report](https://gitlab.com/invenstorypepew/invenstoryppw/badges/master/coverage.svg)](https://gitlab.com/invenstorypepew/invenstoryppw/commits/master)


Nama Anggota Kelompok:
1. Muhamad Faarih Ihsan : Add Product
2. Muhamad Raihan Fikriansyah : Produk Terjual
3. Anatasya Dwijayanti : Produk List
4. Rifqi Hilman Saputra : Add Inventory

Link Herokuapp : invenstorypepew.herokuapp.com

Cerita aplikasi yang diajukan serta kebermanfaatannya:

Jadi kelompok kami membuat suatu website yang menyediakan inventory online suatu 
restoran. Aplikasi ini berguna untuk mengontrol ketersediaan barang yang ada di 
restoran tersebut. Suatu restoran pasti memiliki banyak produk sehingga pasti 
kewalahan untuk mengecek stok barang secara manual.Dengan menggunakan website 
yang telah kami buat, mereka bisa mengetahui suatu produk membutuhkan bahan apa 
saja, berapa roduk tersebut yang sudah terjual dan disesuaikan dengan 
ketersediaan barang yang ada pada di restoran tersebut. Jadi restoran ini bisa 
menambah stok barang jika ketersediaan barang sudah habis atau sudah tidak 
mencukupi untuk membuat suatu produk tersebut lagi

Daftar fitur yang diimplementasikan :


Fitur yang pertama Produk List adalah berisi produk produk apa saja yang ada
restoran tersebut serta bahan yang dibutuhkan tiap produk.
Fitur yang kedua Add Produk adalah produk dengan bahan bahan apa saja yang diperlukan untuk 
membuat produk tersebut.
Fitur yang ketiga adalah bahan bahan apa saja serta jumlah yang dmiliki oleh 
restoran tersebut .
Fitur yang keempat adalah jumlah produk produk yang sudah terjual 
ada berapa.


