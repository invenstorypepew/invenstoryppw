from django.shortcuts import render
from inven.forms import AddProduct, AddBahan
from inven.models import Makanan, BahanMakanan


def productlist(request):
	makanan = Makanan.objects.all();
	bahanMakanan = BahanMakanan.objects.all();
	content = {'makanan' : makanan,
				'bahanMakanan' : bahanMakanan,
				}
	return render(request, 'productlist.html',content)