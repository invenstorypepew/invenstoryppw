from django.test import TestCase, Client


class productlistTest(TestCase):
	def test_productlist_url_is_exist(self):
		response = Client().get('/productlist/productlist')
		self.assertEqual(response.status_code,200)
	def test_productlist_using_productlist_templates(self):
		response = Client().get('/productlist/productlist')
		self.assertTemplateUsed(response, 'productlist.html')