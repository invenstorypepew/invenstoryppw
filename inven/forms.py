from . import models
from django import forms
from django.forms import formset_factory

class AddProduct(forms.Form):
    makanan = forms.CharField(label='Product Name', widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'type' : 'text',
    }), max_length=100)

class AddBahan(forms.Form):
    bahan = forms.CharField(label='Ingredients', widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'type' : 'text',
    }), max_length=100)
    jumlah = forms.IntegerField(label='Quantity', widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'type' : 'text',
    }))

Formset = formset_factory(AddBahan, extra=1)