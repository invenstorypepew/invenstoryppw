from django.test import TestCase, Client
from .models import BahanMakanan, Makanan
from .forms import Formset


class UnitTestAddProduct(TestCase):
    def test_url_is_exist(self):
        response = self.client.get('/inventory/add/')
        self.assertEqual(response.status_code, 200)

    def test_ada_object(self):
        mak = Makanan(makanan='kue padang')
        mak.save()
        ing = BahanMakanan(bahan='telur', jumlah=20)
        ing.save()
        ing.makanan.add(mak)
        self.assertEqual(ing.bahan, 'telur')
        self.assertEqual(ing.jumlah, 20)
        self.assertEqual(mak.makanan, 'kue padang')
    
    def test_save_lewat_html(self):
        data={
            'form-TOTAL_FORMS': 1, 
            'form-INITIAL_FORMS': 1,
            'form-MIN_NUM_FORMS': ['0'], 
            'form-MAX_NUM_FORMS': ['1000'], 
            'form-0-bahan': ['kue medan'], 
            'form-0-jumlah': ['1'],
            'makanan' : 'kue medan',
            'bahan' : 'telur',
            'jumlah' : 20
        }
        response = Client().post('/inventory/add/', data )
        self.assertEqual(response.status_code, 302)
        mak = Makanan.objects.get(makanan = 'kue medan')
        self.assertEqual(mak.makanan, 'kue medan')
    
