from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Makanan, BahanMakanan
from .forms import AddProduct, Formset, AddBahan

def add(request):
    if request.method == 'GET':
        formset = Formset(request.GET or None)
        form = AddProduct()
    elif request.method == "POST" :
        form = AddProduct(request.POST)
        formset = Formset(request.POST)
        if formset.is_valid() and form.is_valid():
            mm = form.cleaned_data['makanan']
            if (Makanan.objects.filter(makanan=mm).count() == 0):
                mak = Makanan(
                    makanan = form.cleaned_data['makanan']
                )
                mak.save()
                i=0
                for form in formset:
                    ing = BahanMakanan(
                        bahan = form.cleaned_data.get('bahan'),
                        jumlah = form.cleaned_data.get('jumlah'),
                    )
                    ing.save()
                    ing.makanan.add(mak)
                    i+=1
            return redirect('inventory:addProduct')
    return render(request, 'inven/add.html', {'formset':formset, 'form':form })

