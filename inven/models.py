from django.db import models

class Makanan(models.Model):
    makanan = models.CharField(max_length=100)
    class Meta:
        ordering = ('makanan',)
    def ambil(self):
        list_bahan_makanan = []
        bahanMakanan = BahanMakanan.objects.all()
        for i in bahanMakanan:
            if(self in i.makanan.all()):
                list_bahan_makanan.append(i)
        return list_bahan_makanan


class BahanMakanan(models.Model):
    bahan = models.CharField(max_length=100)
    jumlah = models.IntegerField()
    makanan = models.ManyToManyField(Makanan)

    class Meta:
        ordering = ('bahan',)
    
